        <h3>Asiakkaat</h3>
        <?php print anchor($uri='asiakas/lisaa', 'Lisää')?>
        <!-- anchor metodilla luodaan <a>-elementti johon tulee linkki lisäyslomakkeelle, linkki näkyy "Lisää" tekstinä-->
        <br>

        <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h4>Etsintäkenttä</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-3">
            <form method="post" action="<?php print site_url() . 'asiakas/index'; ?>" class="search-form">
                <div class="form-group has-feedback">
            		<label for="search" class="sr-only">Etsi asiakas</label>
            		<input type="text" class="form-control" name="search" id="search" placeholder="Etsi asiakas..">
              		<span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </form>
        </div>
    </div>
        <br>
        <table class="table table-striped">
            <tr>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/id", "Id"); ?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/etunimi", "Etunimi"); ?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/sukunimi", "Sukunimi"); ?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/osoite", "Osoite"); ?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/postitmp", "Postitoimipaikka"); ?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/postinro", "Postinumero"); ?></th>
                <th>Poisto</th>
                <th>Muokkaus</th>
                <th>Muistiinpanot</th>
            </tr>
        <?php
        // put your code here
        foreach($asiakkaat as $asiakas) {            
            print "<tr><td>$asiakas->id</td>"
                    . "<td>$asiakas->etunimi</td> "
                    . "<td>$asiakas->sukunimi</td> "
                    . "<td>$asiakas->osoite</td> "
                    . "<td>$asiakas->postitmp</td> "
                    . "<td>$asiakas->postinro</td> "
                    . "<td>" . anchor("asiakas/poista/$asiakas->id", "Poista") . "</td> "
                    . "<td>" . anchor("asiakas/muokkaa/$asiakas->id", "Muokkaa") . "</td>"
                    . "<td>" . anchor("muistio/index/$asiakas->id", "Muistiinpanot") . "</td></tr>";
        }
            ?>
        </table>    
        <?php
            echo $this->pagination->create_links();
        ?>