   
<div class="row">
    <div class="col-xs-12 col-md-offset-4 col-md-4">
        <h2>Rekisteröityminen</h2>
        <?php 
            print validation_errors("<p style='color:red'>","</p>");
        ?>
        <form role="form" action="<?php print site_url() . '/kayttaja/rekisteroidy';?>" method="post">
            <div class="form-group">
            <label for="tunnus">Käyttäjätunnus:</label>
            <input type="email" class="form-control" name="tunnus">
            </div><div class="form-group">
            <label for="salasana">Salasana:</label>
            <input type="password" class="form-control" name="salasana" >
            </div><div class="form-group">
            <label for="salasana2">Syötä salasana uudelleen:</label>
            <input type="password" class="form-control" name="salasana2">
            </div>
            <button class="btn btn-primary">Rekisteröidy</button>
            <a class="btn" href="<?php print site_url() . '/kayttaja/index';?>">&nbsp;Kirjaudu</a>
       </form>
    </div>
</div>
        <?php
        // put your code here
        ?>