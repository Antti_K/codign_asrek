/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  n5nisa00
 * Created: Jan 10, 2017
 */

drop database if exists asiakasrekisteri;
create database asiakasrekisteri;
use asiakasrekisteri;

create table asiakas (
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    lahiosoite varchar(50),
    postitoimipaikka varchar(50),
    postinumero varchar(5)
);

insert into asiakas(etunimi, sukunimi) values ('Santtu', 'Niskala');
insert into asiakas(etunimi, sukunimi) values ('toinen', 'henkilö');