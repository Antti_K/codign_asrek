<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kayttaja extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('kayttaja_model');
        $this->load->library('form_validation');
        $this->load->library('encrypt');
    }
    public function index() {
        $data['main_content'] = 'kirjaudu_view';
        $this->load->view('template', $data);
    }

    public function kirjaudu(){
        //syötetarkistus
        $tunnus = $this->input->post('tunnus');
        $salasana = $this->input->post('salasana');
             
        $this->form_validation->set_rules('tunnus','tunnus','required|valid_email');
        $this->form_validation->set_rules('salasana','salasana','required|min_length[8]');
        $this->form_validation->set_rules('tunnus','tunnus','callback_tarkasta_kayttaja');
        
        if ($this->form_validation->run() === TRUE){           
            //ohjaus asiakasrekisteriin
            
            redirect('asiakas/index');
        }                  
        //jos syöte väärin, mennään kirjaudu-sivulle
        else {
            $data['main_content'] = 'kirjaudu_view';
            $this->load->view('template', $data);
        }         
    }
    
    public function tarkasta_kayttaja() {
         //otetaan syötetyt tiedot talteen
        $tunnus = $this->input->post('tunnus');
        $salasana = $this->input->post('salasana');
        
        $kayttaja = $this->kayttaja_model->tarkasta_kayttaja($tunnus,$salasana);
        
        if ($kayttaja){
             //luodaan istuntomuuttuja, sen nimi on kayttaja(vapaasti valittavissa), 
            //muuttujan sisältö on koko $kayttaja -olio
            $this->session->set_userdata('kayttaja',$kayttaja);
            //jos käyttäjä löytyy
            return TRUE;
        }
        //jos ei niin false
        else {
            $this->form_validation->set_message('tarkasta_kayttaja','Käyttäjätunnus tai salasana virheellinen');
            return FALSE;
        }
    }
    
    public function rekisteroityminen() {
        $data['main_content'] = 'rekisteroidy_view';
        $this->load->view('template', $data);
    }
    public function rekisteroidy() {
        $data = array(
            //tietokannan kenttä => lomakkeen kentän nimi
            'email' => $this->input->post('tunnus'),
            'salasana' => $this->encrypt->encode($this->input->post('salasana'))
                //encrypt->encode salaa salasanan
        );
        $this->form_validation->set_rules('tunnus', 'tunnus', 'required|valid_email');
                //set_rules('1', '2', '3'); 1.kohta liittää validoinnin johonkin kenttään, 
                //2. kohta on kentän selkokielinen nimi esim. virheilmoitukseen, 
                //3. kenttä on validointiehto/ehdot 
        $this->form_validation->set_rules('salasana', 'salasana', 'required|min_length[8]');
        $this->form_validation->set_rules('salasana2', 'salasanan vahvistus', 'trim|required|matches[salasana]');
        if ($this->form_validation->run() === TRUE) {
            $this->kayttaja_model->lisaa($data);
            redirect('/kayttaja/index');
            } 
        else {
            $data['main_content'] = 'rekisteroidy_view';
            $this->load->view('template', $data);
        }
    }
    public function kirjaudu__ulos() {
        //tuhotaan sessiomuuttuja
        $this->session->unset_userdata('kayttaja');
        $this->session->sess_destroy();
        //mennään kirjautumissivulle, refresh estää välimuistista vanhan käyttäjän tietojen hakemisen.
        redirect('/kayttaja/index','refresh');

}
}
?>
