
        <!--formille haetaan php-koodi osoitteesta sivun osoite + /asiakas/tallenna-->
        <h3>Lisää asiakas</h3>
        
        <?php 
            print validation_errors("<p style='color:red'>","</p>");
        ?>
        
        <form action="<?php print site_url() . '/asiakas/tallenna';?>" method="post">
            <input type="hidden" name="id" value="<?php print $id;?>"
            <div class="form-group">
                <label >Sukunimi</label>
                <?php echo form_error('sukunimi')?>
                <input class="form-control" name="sukunimi" value="<?php print $sukunimi;?>"  >
            </div>
            <div class="form-group">
                <label>Etunimi</label>
                <input class="form-control" name="etunimi" value="<?php print $etunimi;?>" >
            </div>
            <div class="form-group">
                <label>Lähiosoite</label>
                <input class="form-control" name="lahiosoite" value="<?php print $lahiosoite;?>" >
            </div>
            <div class="form-group">
                <label>Postitoimipaikka</label>
                <input class="form-control" name="postitoimipaikka" value="<?php print $postitoimipaikka;?>" >
            </div>
            <div class="form-group">
                <label>Postinumero</label>
                <input class="form-control" name="postinumero" value="<?php print $postinumero;?>" >
            </div>
            <div class="form-group">
                <button class="form-control">Tallenna</button>
            </div>
        </form>
   
       
