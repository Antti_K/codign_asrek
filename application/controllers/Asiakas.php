<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Asiakas extends CI_Controller {

    public function __construct() {
        parent::__construct();

        //lisätään istuntomuuttujan tarkistus, estetään kirjautumattomien käyttäjien pääsyn tietoihin
        if (!isset($_SESSION['kayttaja'])){
            //jos ei ole kirjautunut käyttäjä, ohjataan kirjautumissivulle
            redirect('kayttaja/index');
        }
        $this->load->library('pagination');    
        $this->load->model('asiakas_model');
        $this->load->library('form_validation');        
    }

    public function index() {
        $asiakkaita_sivulla = 2;
        $jarjestys="";
        if ($this->session->userdata("jarjestys")){
            $jarjestys=$this->session->userdata("jarjestys");
        }
        if ($this->uri->segment(4)){
            $jarjestys=$this->uri->segment(4);
            $this->session->set_userdata("jarjestys",$jarjestys);
        }
        $etsi = ""; //samanniminen, muttei sama muuttuja kuin modelissa.
        $etsi = $this->input->post("search");  
        $data['asiakkaat']=$this->asiakas_model->hae_kaikki($etsi, $asiakkaita_sivulla, $this->uri->segment(3), $jarjestys);
        $data["sivutuksen_kohta"]=0;
        $config['base_url']=site_url('asiakas/index');
        $config['total_rows']=$this->asiakas_model->laske_asiakkaat();
        $config['per_page']=$asiakkaita_sivulla;
        $config['uri_segment']=3;
        $this->pagination->initialize($config);
                
        $data['main_content'] = 'asiakkaat_view';
        $this->load->view('template', $data);
    }

    public function lisaa() {
        $data = array(
            'id' => '',
            'sukunimi' => '',
            'etunimi' => '',
            'lahiosoite' => '',
            'postitoimipaikka' => '',
            'postinumero' => ''
        );


        $data['main_content'] = 'asiakas_view';
        $this->load->view('template', $data);
    }

    public function muokkaa($id) {
        $asiakas = $this->asiakas_model->hae($id);

        if (isset($asiakas)) {

            $data = array(
                'id' => $asiakas->id,
                'sukunimi' => $asiakas->sukunimi,
                'etunimi' => $asiakas->etunimi,
                'lahiosoite' => $asiakas->osoite,
                'postitoimipaikka' => $asiakas->postitmp,
                'postinumero' => $asiakas->postinro
            );


            $data['main_content'] = 'asiakas_view';
            $this->load->view('template', $data);
        } else {
            throw new Exception('Asiakasta ei löydy');
        }
    }

    public function tallenna() {
        $data = array(
            //tietokannan kenttä => lomakkeen kentän nimi
            'id' => $this->input->post('id'),
            'sukunimi' => $this->input->post('sukunimi'),
            'etunimi' => $this->input->post('etunimi'),
            'osoite' => $this->input->post('lahiosoite'),
            'postitmp' => $this->input->post('postitoimipaikka'),
            'postinro' => $this->input->post('postinumero')
        );

        $this->form_validation->set_rules('etunimi', 'etunimi', 'required');
        $this->form_validation->set_rules('sukunimi', 'sukunimi', 'required');
        if ($this->form_validation->run() === TRUE) {
            if (strlen($this->input->post('id')) === 0) {
                $this->asiakas_model->lisaa($data);
                
                } 
            else {
                $this->asiakas_model->muokkaa($data);
            }
            redirect('/asiakas/index');
        } 
        else {
            $data['main_content'] = 'asiakas_view';
            $this->load->view('template', $data);
        }
    }

    public function poista($id) {
        $this->asiakas_model->poista(intval($id));
        redirect('/asiakas/index');
        //$this->index();   tällä tavalla osoite riville jää lukemaan ".../poista/[id]"
    }

}
