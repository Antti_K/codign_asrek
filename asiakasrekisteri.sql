/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  n5kuan00
 * Created: Jan 10, 2017
 */

drop database if exists asiakasrekisteri;

create database asiakasrekisteri;

use asiakasrekisteri;

create table asiakas(
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    osoite varchar(255),
    postitmp varchar(50),
    postinro varchar(5)
);

create table muistio(
    id int primary key auto_increment,
    tallennettu TIMESTAMP default current_timestamp,
    teksti text not null,
    asiakas_id int not null,
    index (asiakas_id),
    foreign key (asiakas_id) references asiakas(id)
    on delete restrict
    kayttaja_id int not null,
    index (kayttaja_id),
    foreign key (kayttaja_id) references kayttaja(id)
    on delete restrict
)

create table kayttaja(
    id int primary key auto_increment,
    email varchar(100) not null unique,
    salasana varchar(255) not null
);

insert into asiakas (etunimi, sukunimi, osoite, postitmp, postinro) 
values ('Antti','Kukkonen', 'Nuolihaukantie 3B 52', 'Oulu', '90250');

insert into asiakas (etunimi, sukunimi, osoite, postitmp, postinro) 
values ('Jouni','Juntunen', 'Katu', 'Paikka', '90109');

insert into asiakas (etunimi, sukunimi, osoite, postitmp, postinro) 
values ('Testi','Henkilö', 'Tie', 'Lokaatio', '10109');
