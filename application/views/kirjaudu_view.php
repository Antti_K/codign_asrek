 <?php 
            print validation_errors("<p style='color:red'>","</p>");
        ?>
<div class="row">
    <div class="col-xs-12 col-md-offset-4 col-md-4">
        <h2>Kirjaudu sisään</h2>
        <form role="form" action="<?php print site_url() . '/kayttaja/kirjaudu';?>" method="post">
            <div class="form-group">
            <label for="tunnus">Käyttäjätunnus:</label>
            <input type="email" class="form-control" name="tunnus" >
            </div><div class="form-group">
            <label for="salasana">Salasana:</label>
            <input type="password" class="form-control" name="salasana">
            </div>
            <button class="btn btn-primary">Kirjaudu</button>
            <a class="btn" href="<?php print site_url() . '/kayttaja/rekisteroityminen';?>">&nbsp;Rekisteröidy</a>
        </form>
    </div>
</div>
<?php
// put your code here
?>